(function(){

    'use strict';

    /*
     * Licensed to the Apache Software Foundation (ASF) under one
     * or more contributor license agreements.  See the NOTICE file
     * distributed with this work for additional information
     * regarding copyright ownership.  The ASF licenses this file
     * to you under the Apache License, Version 2.0 (the
     * "License"); you may not use this file except in compliance
     * with the License.  You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing,
     * software distributed under the License is distributed on an
     * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     * KIND, either express or implied.  See the License for the
     * specific language governing permissions and limitations
     * under the License.
     */
    app = {
        // Application Constructor
        initialize: function() {
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function() {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicitly call 'app.receivedEvent(...);'
        onDeviceReady: function() {
            app.fetchFeed( app.buildFeed );
        },
        feed: null,
        feedItems: [],
        fetchFeed: function( _cb ){

            var callback = _cb || function(){};

            $.get('https://api.selesti.com/cors-plz?mirror=https://rss.acast.com/nrf1')
            .done(function(resp){

                try {
                    app.feed = $(resp).find('item');
                    callback(app.feed);
                }
                catch(exception){
                    alert('try fail');
                    console.error(exception);
                }
            })
            .fail(function(resp, b, c){
                console.error(resp);
            });
        },
        buildFeed: function(){

            var feedUl       = $('#listView'),
                feedTemplate = feedUl.find('.template-row');

            //Clean up the old feed items
            if( app.feedItems.length ){
                app.feedItems.remove();
            }

            app.feedItems = [];

            //Add new feed items to the list
            for(var i = 0; i < app.feed.length; i++){

                var xml  = $(app.feed[i]),
                    item = feedTemplate.clone();

                    item.xml      = app.feed[i];
                    item.h1       = xml.find('title').text();
                    item.desc     = xml.find('itunes\\:subtitle, subtitle').text() || xml.find('description').text();
                    item.date     = xml.find('pubDate').text();
                    item.duration = xml.find('duration').text();
                    item.image    = xml.find('itunes\\:image, image').attr('href') || '/img/logo.png';
                    item.url      = xml.find('link').text();
                    item.mp3      = xml.find('enclosure').attr('url');
                    item.state    = 0;
                    item.player   = null;

                item.removeClass('template-row');

                item.find('img').attr({src: item.image, alt: item.h1});
                item.find('h1').text( item.h1 );
                item.find('time').text( item.duration );
                item.find('p').text( item.desc );
                item.find('a.open-webpage').attr('href', item.url);
                item.find('a.play-mp3').attr('href', item.mp3).on('click', item, function(evt){

                    evt.preventDefault();
                    var i = evt.data;

                    if( !i.player ){

                        i.player = new Media(i.mp3, app.playbackComplete, app.playbackError, function(state){

                            i.state = state;

                            var params = ['NR F1', i.h1, i.h1, i.image, 30, 10];

                            //$('h2').text( JSON.stringify(params) );

                            window.remoteControls.updateMetas(function(s){
                                $('h2').text( JSON.stringify(s) );
                            }, function(e){
                                $('h2').text( JSON.stringify(e) );
                            }, params);
                        });
                    }

                    document.addEventListener('remote-event', function(e){
                        alert(e);
                    });

                    i.player.play();
                });

                item.appendTo(feedUl);
                app.feedItems.push(item);
            }

            item = null;
        },
        playbackComplete: function(){
            console.log('User Y finished listening to X');
        },
        playbackError: function(){
            console.log('User Y recieved error when trying to listn to X');
        }
    };

    app.initialize();

})();

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7XG5cbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICAvKlxuICAgICAqIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiAgICAgKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiAgICAgKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuICAgICAqIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiAgICAgKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4gICAgICogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4gICAgICogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICAgICAqXG4gICAgICogaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gICAgICpcbiAgICAgKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4gICAgICogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiAgICAgKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuICAgICAqIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuICAgICAqIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiAgICAgKiB1bmRlciB0aGUgTGljZW5zZS5cbiAgICAgKi9cbiAgICBhcHAgPSB7XG4gICAgICAgIC8vIEFwcGxpY2F0aW9uIENvbnN0cnVjdG9yXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgICAgIH0sXG4gICAgICAgIC8vIEJpbmQgRXZlbnQgTGlzdGVuZXJzXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEJpbmQgYW55IGV2ZW50cyB0aGF0IGFyZSByZXF1aXJlZCBvbiBzdGFydHVwLiBDb21tb24gZXZlbnRzIGFyZTpcbiAgICAgICAgLy8gJ2xvYWQnLCAnZGV2aWNlcmVhZHknLCAnb2ZmbGluZScsIGFuZCAnb25saW5lJy5cbiAgICAgICAgYmluZEV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdkZXZpY2VyZWFkeScsIHRoaXMub25EZXZpY2VSZWFkeSwgZmFsc2UpO1xuICAgICAgICB9LFxuICAgICAgICAvLyBkZXZpY2VyZWFkeSBFdmVudCBIYW5kbGVyXG4gICAgICAgIC8vXG4gICAgICAgIC8vIFRoZSBzY29wZSBvZiAndGhpcycgaXMgdGhlIGV2ZW50LiBJbiBvcmRlciB0byBjYWxsIHRoZSAncmVjZWl2ZWRFdmVudCdcbiAgICAgICAgLy8gZnVuY3Rpb24sIHdlIG11c3QgZXhwbGljaXRseSBjYWxsICdhcHAucmVjZWl2ZWRFdmVudCguLi4pOydcbiAgICAgICAgb25EZXZpY2VSZWFkeTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBhcHAuZmV0Y2hGZWVkKCBhcHAuYnVpbGRGZWVkICk7XG4gICAgICAgIH0sXG4gICAgICAgIGZlZWQ6IG51bGwsXG4gICAgICAgIGZlZWRJdGVtczogW10sXG4gICAgICAgIGZldGNoRmVlZDogZnVuY3Rpb24oIF9jYiApe1xuXG4gICAgICAgICAgICB2YXIgY2FsbGJhY2sgPSBfY2IgfHwgZnVuY3Rpb24oKXt9O1xuXG4gICAgICAgICAgICAkLmdldCgnaHR0cHM6Ly9hcGkuc2VsZXN0aS5jb20vY29ycy1wbHo/bWlycm9yPWh0dHBzOi8vcnNzLmFjYXN0LmNvbS9ucmYxJylcbiAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uKHJlc3Ape1xuXG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgYXBwLmZlZWQgPSAkKHJlc3ApLmZpbmQoJ2l0ZW0nKTtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soYXBwLmZlZWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjYXRjaChleGNlcHRpb24pe1xuICAgICAgICAgICAgICAgICAgICBhbGVydCgndHJ5IGZhaWwnKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihleGNlcHRpb24pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuZmFpbChmdW5jdGlvbihyZXNwLCBiLCBjKXtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKHJlc3ApO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICAgIGJ1aWxkRmVlZDogZnVuY3Rpb24oKXtcblxuICAgICAgICAgICAgdmFyIGZlZWRVbCAgICAgICA9ICQoJyNsaXN0VmlldycpLFxuICAgICAgICAgICAgICAgIGZlZWRUZW1wbGF0ZSA9IGZlZWRVbC5maW5kKCcudGVtcGxhdGUtcm93Jyk7XG5cbiAgICAgICAgICAgIC8vQ2xlYW4gdXAgdGhlIG9sZCBmZWVkIGl0ZW1zXG4gICAgICAgICAgICBpZiggYXBwLmZlZWRJdGVtcy5sZW5ndGggKXtcbiAgICAgICAgICAgICAgICBhcHAuZmVlZEl0ZW1zLnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhcHAuZmVlZEl0ZW1zID0gW107XG5cbiAgICAgICAgICAgIC8vQWRkIG5ldyBmZWVkIGl0ZW1zIHRvIHRoZSBsaXN0XG4gICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgYXBwLmZlZWQubGVuZ3RoOyBpKyspe1xuXG4gICAgICAgICAgICAgICAgdmFyIHhtbCAgPSAkKGFwcC5mZWVkW2ldKSxcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IGZlZWRUZW1wbGF0ZS5jbG9uZSgpO1xuXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0ueG1sICAgICAgPSBhcHAuZmVlZFtpXTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5oMSAgICAgICA9IHhtbC5maW5kKCd0aXRsZScpLnRleHQoKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5kZXNjICAgICA9IHhtbC5maW5kKCdpdHVuZXNcXFxcOnN1YnRpdGxlLCBzdWJ0aXRsZScpLnRleHQoKSB8fCB4bWwuZmluZCgnZGVzY3JpcHRpb24nKS50ZXh0KCk7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0uZGF0ZSAgICAgPSB4bWwuZmluZCgncHViRGF0ZScpLnRleHQoKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5kdXJhdGlvbiA9IHhtbC5maW5kKCdkdXJhdGlvbicpLnRleHQoKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5pbWFnZSAgICA9IHhtbC5maW5kKCdpdHVuZXNcXFxcOmltYWdlLCBpbWFnZScpLmF0dHIoJ2hyZWYnKSB8fCAnL2ltZy9sb2dvLnBuZyc7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0udXJsICAgICAgPSB4bWwuZmluZCgnbGluaycpLnRleHQoKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5tcDMgICAgICA9IHhtbC5maW5kKCdlbmNsb3N1cmUnKS5hdHRyKCd1cmwnKTtcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5zdGF0ZSAgICA9IDA7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0ucGxheWVyICAgPSBudWxsO1xuXG4gICAgICAgICAgICAgICAgaXRlbS5yZW1vdmVDbGFzcygndGVtcGxhdGUtcm93Jyk7XG5cbiAgICAgICAgICAgICAgICBpdGVtLmZpbmQoJ2ltZycpLmF0dHIoe3NyYzogaXRlbS5pbWFnZSwgYWx0OiBpdGVtLmgxfSk7XG4gICAgICAgICAgICAgICAgaXRlbS5maW5kKCdoMScpLnRleHQoIGl0ZW0uaDEgKTtcbiAgICAgICAgICAgICAgICBpdGVtLmZpbmQoJ3RpbWUnKS50ZXh0KCBpdGVtLmR1cmF0aW9uICk7XG4gICAgICAgICAgICAgICAgaXRlbS5maW5kKCdwJykudGV4dCggaXRlbS5kZXNjICk7XG4gICAgICAgICAgICAgICAgaXRlbS5maW5kKCdhLm9wZW4td2VicGFnZScpLmF0dHIoJ2hyZWYnLCBpdGVtLnVybCk7XG4gICAgICAgICAgICAgICAgaXRlbS5maW5kKCdhLnBsYXktbXAzJykuYXR0cignaHJlZicsIGl0ZW0ubXAzKS5vbignY2xpY2snLCBpdGVtLCBmdW5jdGlvbihldnQpe1xuXG4gICAgICAgICAgICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgaSA9IGV2dC5kYXRhO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKCAhaS5wbGF5ZXIgKXtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaS5wbGF5ZXIgPSBuZXcgTWVkaWEoaS5tcDMsIGFwcC5wbGF5YmFja0NvbXBsZXRlLCBhcHAucGxheWJhY2tFcnJvciwgZnVuY3Rpb24oc3RhdGUpe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaS5zdGF0ZSA9IHN0YXRlO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHBhcmFtcyA9IFsnTlIgRjEnLCBpLmgxLCBpLmgxLCBpLmltYWdlLCAzMCwgMTBdO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8kKCdoMicpLnRleHQoIEpTT04uc3RyaW5naWZ5KHBhcmFtcykgKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5yZW1vdGVDb250cm9scy51cGRhdGVNZXRhcyhmdW5jdGlvbihzKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnaDInKS50ZXh0KCBKU09OLnN0cmluZ2lmeShzKSApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCdoMicpLnRleHQoIEpTT04uc3RyaW5naWZ5KGUpICk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgcGFyYW1zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigncmVtb3RlLWV2ZW50JywgZnVuY3Rpb24oZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGVydChlKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgaS5wbGF5ZXIucGxheSgpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgaXRlbS5hcHBlbmRUbyhmZWVkVWwpO1xuICAgICAgICAgICAgICAgIGFwcC5mZWVkSXRlbXMucHVzaChpdGVtKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaXRlbSA9IG51bGw7XG4gICAgICAgIH0sXG4gICAgICAgIHBsYXliYWNrQ29tcGxldGU6IGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnVXNlciBZIGZpbmlzaGVkIGxpc3RlbmluZyB0byBYJyk7XG4gICAgICAgIH0sXG4gICAgICAgIHBsYXliYWNrRXJyb3I6IGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnVXNlciBZIHJlY2lldmVkIGVycm9yIHdoZW4gdHJ5aW5nIHRvIGxpc3RuIHRvIFgnKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBhcHAuaW5pdGlhbGl6ZSgpO1xuXG59KSgpO1xuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
