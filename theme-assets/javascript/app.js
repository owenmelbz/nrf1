(function(){

    'use strict';

    /*
     * Licensed to the Apache Software Foundation (ASF) under one
     * or more contributor license agreements.  See the NOTICE file
     * distributed with this work for additional information
     * regarding copyright ownership.  The ASF licenses this file
     * to you under the Apache License, Version 2.0 (the
     * "License"); you may not use this file except in compliance
     * with the License.  You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing,
     * software distributed under the License is distributed on an
     * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     * KIND, either express or implied.  See the License for the
     * specific language governing permissions and limitations
     * under the License.
     */
    app = {
        // Application Constructor
        initialize: function() {
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function() {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicitly call 'app.receivedEvent(...);'
        onDeviceReady: function() {
            app.fetchFeed( app.buildFeed );
        },
        feed: null,
        feedItems: [],
        fetchFeed: function( _cb ){

            var callback = _cb || function(){};

            $.get('https://api.selesti.com/cors-plz?mirror=https://rss.acast.com/nrf1')
            .done(function(resp){

                try {
                    app.feed = $(resp).find('item');
                    callback(app.feed);
                }
                catch(exception){
                    alert('try fail');
                    console.error(exception);
                }
            })
            .fail(function(resp, b, c){
                console.error(resp);
            });
        },
        buildFeed: function(){

            var feedUl       = $('#listView'),
                feedTemplate = feedUl.find('.template-row');

            //Clean up the old feed items
            if( app.feedItems.length ){
                app.feedItems.remove();
            }

            app.feedItems = [];

            //Add new feed items to the list
            for(var i = 0; i < app.feed.length; i++){

                var xml  = $(app.feed[i]),
                    item = feedTemplate.clone();

                    item.xml      = app.feed[i];
                    item.h1       = xml.find('title').text();
                    item.desc     = xml.find('itunes\\:subtitle, subtitle').text() || xml.find('description').text();
                    item.date     = xml.find('pubDate').text();
                    item.duration = xml.find('duration').text();
                    item.image    = xml.find('itunes\\:image, image').attr('href') || '/img/logo.png';
                    item.url      = xml.find('link').text();
                    item.mp3      = xml.find('enclosure').attr('url');
                    item.state    = 0;
                    item.player   = null;

                item.removeClass('template-row');

                item.find('img').attr({src: item.image, alt: item.h1});
                item.find('h1').text( item.h1 );
                item.find('time').text( item.duration );
                item.find('p').text( item.desc );
                item.find('a.open-webpage').attr('href', item.url);
                item.find('a.play-mp3').attr('href', item.mp3).on('click', item, function(evt){

                    evt.preventDefault();
                    var i = evt.data;

                    if( !i.player ){

                        i.player = new Media(i.mp3, app.playbackComplete, app.playbackError, function(state){

                            i.state = state;

                            var params = ['NR F1', i.h1, i.h1, i.image, 30, 10];

                            //$('h2').text( JSON.stringify(params) );

                            window.remoteControls.updateMetas(function(s){
                                $('h2').text( JSON.stringify(s) );
                            }, function(e){
                                $('h2').text( JSON.stringify(e) );
                            }, params);
                        });
                    }

                    document.addEventListener('remote-event', function(e){
                        alert(e);
                    });

                    i.player.play();
                });

                item.appendTo(feedUl);
                app.feedItems.push(item);
            }

            item = null;
        },
        playbackComplete: function(){
            console.log('User Y finished listening to X');
        },
        playbackError: function(){
            console.log('User Y recieved error when trying to listn to X');
        }
    };

    app.initialize();

})();
